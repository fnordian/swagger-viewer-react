import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { login, logout } from '@fuglu/redux-implicit-oauth2'

const config = {
    url: null,
    client: "",
    redirect: window.location,
    scope: "all"
};

const Login = ({ isLoggedIn, login, logout, security, client }) => {
    const doLogin = () => {
        login({...config, url: security.authorizationUrl, client});
    };

    if (isLoggedIn) {
        return <button type='button' onClick={logout}>Logout</button>
    } else {
        return <button type='button' onClick={doLogin}>Login</button>
    }
};

Login.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    login: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired
};

const mapStateToProps = ({ auth }) => ({
    isLoggedIn: auth.isLoggedIn
});

const mapDispatchToProps = {
    login,
    logout
};

export default connect(mapStateToProps, mapDispatchToProps)(Login)
