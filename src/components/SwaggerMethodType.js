import React, {Component} from 'react';
import 'rc-collapse/assets/index.css';
import Collapse, {Panel} from 'rc-collapse';
import PropTypes from 'prop-types';
import '../Swagger.css';
import {Container, Col, Row, Button} from 'reactstrap';
import {valueType} from '../helper/swaggerExample';
import InfoPanel from "./InfoPanel";

const enumOrClass = (c, e) =>
    e
        ? `${c}-enum (` + e.map((s) => `"${s}"`).join(", ") + ")"
        : c;

class SwaggerMethodTypePrimitive extends Component {
    static PropTypes = {
        typeClass: PropTypes.string.isRequired,
        typeName: PropTypes.string.isRequired,
        typeEnum: PropTypes.string.isRequired
    };

    resolveClass() {
        return enumOrClass(this.props.typeClass, this.props.typeEnum);
    }

    render() {
        return <div className="primitiveType">
            <span className="elementType">{this.resolveClass()}</span>
        </div>
    }
}

class SwaggerMethodTypeArray extends Component {
    static PropTypes = {
        items: PropTypes.object.isRequired,
        typeName: PropTypes.string.isRequired,
        definitions: PropTypes.object.isRequired
    };

    render() {
        return <div className="arrayType">
            <span className="elementType"><SwaggerMethodTypeExpandable type={this.props.items}
                                                                                definitions={this.props.definitions}
                                                                                expanded={false}/></span>
        </div>
    }
}


class SwaggerMethodTypeExpandableItem extends Component {


    renderRows() {
        return [<Row key="0">
            <Col md={6}>
                <b className="float-right">{this.props.name}</b>
            </Col>
            <Col md={6}>
                {this.props.description ? <p>{this.props.description}<br /></p> : null}
                {this.props.typeName}
            </Col>

        </Row>,
            (this.props.children) ? <Row key="1">
                <Col>
                    <InfoPanel>
                        {this.props.children}
                    </InfoPanel>
                </Col>
            </Row>
                : null
        ]
    }

    render() {
        return <Container fluid style={{
            borderBottom: "1px solid #DDDDDD",
            paddingTop: "10px",
            paddingBottom: "10px",
            paddingLeft: "0px",
            paddingRight: "0px"
        }}>{this.renderRows()}</Container>;
    }
}

class SwaggerMethodTypeObject extends Component {
    static PropTypes = {
        typeName: PropTypes.string.isRequired,
        properties: PropTypes.object.isRequired,
        additionalProperties: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired,
        expanded: PropTypes.bool.isRequired,
    };

    componentWillMount() {
        this.setState({expanded: this.props.expanded, collapseAble: !this.props.expanded});
    }

    isExpandableType(type) {
        return (type.type === "object"
        || (
        type.type === "array"
        && (
        valueType({definitions: this.props.definitions}, type.items).type === "array"
        || valueType({definitions: this.props.definitions}, type.items).type === "object")));
    }

    descriptiveTypeName(type) {
        if (type.type === "array") {
            let aType = valueType({definitions: this.props.definitions}, type.items);
            return "array" + (aType ? " of " + aType.type : "");
        } else {
            return enumOrClass(type.type, type.enum);
        }
    }

    renderProperties() {
        const properties = {...this.props.properties, ...this.props.additionalProperties};
        return Object.keys(properties).map((propertyKey) => {
            const property = properties[propertyKey];
            const type = valueType({definitions: this.props.definitions}, property);

            return <SwaggerMethodTypeExpandableItem key={propertyKey} name={propertyKey} description={property.description}
                                                    typeName={this.descriptiveTypeName(type)}>
                {(this.isExpandableType(type))
                    ? <SwaggerMethodTypeExpandable typeName={propertyKey} type={property}
                                                   definitions={this.props.definitions} expanded={false}/>
                    : null
                }

            </SwaggerMethodTypeExpandableItem>

            return [
                <Col md={6} className="objectTypeName">{propertyKey}</Col>
                ,
                <Col md={6}>
                    <SwaggerMethodTypeExpandable typeName={propertyKey} type={property}
                                                 definitions={this.props.definitions} expanded={false}/>
                </Col>
            ]

        });
    }

    curlyLeft = () => '{';
    curlyRight = () => '}';

    render() {
        return <Container fluid>
            {this.renderProperties()}
        </Container>
    }
}


class SwaggerMethodTypeExpandable extends Component {
    static PropTypes = {
        typeName: PropTypes.string.isRequired,
        type: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired,
        expanded: PropTypes.bool.isRequired
    };

    referencedDefinition() {
        const getRef = (refText) => {
            const definitionMatch = /^\#\/definitions\/(.*)$/.exec(refText);
            const definitionName = definitionMatch ? definitionMatch[1] : null;

            return this.props.definitions[definitionName];
        };

        if (this.props.type.schema) {

            if (this.props.type.schema['$ref']) {
                return getRef(this.props.type.schema['$ref']);
            } else if (this.props.type.schema) {
                return this.props.type.schema;
            }
        } else if (this.props.type["$ref"]) {
            return getRef(this.props.type["$ref"]);
        }

        return null;
    }

    render() {
        const inVal = this.props.type.in;
        const type = valueType({definitions: this.props.definitions}, this.props.type);
        const typeRender = () => {
            switch (type.type) {
                case "integer":
                case "boolean":
                case "string":
                    return <SwaggerMethodTypePrimitive typeClass={type.type} typeName={this.props.typeName}
                                                       typeEnum={type.enum}/>;
                case "array":
                    return <SwaggerMethodTypeArray typeName={this.props.typeName} items={type.items}
                                                   definitions={this.props.definitions}/>;
                case "object":
                    return <SwaggerMethodTypeObject typeName={this.props.typeName} properties={type.properties}
                                                    additionalProperties={type.additionalProperties}
                                                    definitions={this.props.definitions}
                                                    expanded={this.props.expanded}/>;
                default:
                    return <SwaggerMethodTypePrimitive typeClass={type.type} typeName={this.props.typeName}/>;
            }

            return <div>no type: {type.type}</div>;
        };

        let foo = <div style={{borderTop: "1px solid #DDDDDD"}}>{typeRender()}</div>

        return typeRender();
    }
}

export const SwaggerMethodType = (props) => {
    return <SwaggerMethodTypeExpandable expanded={true} typeName={props.typeName} type={props.type}
                                        definitions={props.definitions}/>;
};

export
default
SwaggerMethodType
