import React, { Component } from 'react';
import { Tooltip  } from 'reactstrap';

export default class Scopes extends Component {
  static propTypes = {
    children: React.PropTypes.node,
  }

  state = {
    open: false,
    random: Math.random().toString(36).substring(7)
  }

  toggle = () => {
    this.setState({
      open: !this.state.open
    });
  }

  render() {
    return (
      <div className="scopes">
        <a id={this.state.random}>Scopes</a>
        <Tooltip placement="bottom" isOpen={this.state.open} target={this.state.random} toggle={this.toggle}>
          {this.props.children}
        </Tooltip>
      </div>
    );
  }
}
