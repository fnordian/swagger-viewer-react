import React, {Component} from "react";
import "../Swagger.css";

class InfoPanel extends Component {


    componentWillMount() {
        this.setState({expanded: false});
    };


    toggleExpanded() {
        this.setState({expanded: !this.state.expanded});
    }

    renderExpandButton() {
        return (this.props.children)
            ? <div style={{display: "inline"}} onClick={() => this.toggleExpanded()}> { this.state.expanded ? <span>hide child attributes</span> :
                <span>show child attributes</span> } </div>
            : null

    }

    render() {
        return <div>

            <div className="infoPanel" style={{textAlign: "center"}}>
                {this.renderExpandButton()}
            </div>
            { this.state.expanded ? <div className="infoPanelContent">
                {this.props.children}
            </div>
                : null
            }
        </div>
    }
}
;

export default InfoPanel;
