import React from "react"
import PropTypes from "prop-types"
import curlify from "../helper/curlify"

export default class Curl extends React.Component {
  static propTypes = {
    spec: PropTypes.object.isRequired,
    operationId: PropTypes.string.isRequired,
    method: PropTypes.string.isRequired,
    pathName: PropTypes.string.isRequired,
    parameters: PropTypes.object.isRequired,
    securities: PropTypes.object.isRequired,
  }

  render() {
    const curl = curlify(
      this.props.spec,
      this.props.operationId,
      this.props.method,
      this.props.pathName,
      this.props.parameters,
      this.props.securities,
    );

    return (
      <pre className={"curl"}>{curl}</pre>
    )
  }
}
