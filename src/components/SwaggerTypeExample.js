import React from 'react';
import JSONPretty from 'react-json-pretty';
import {exampleParameters, inDescription, sortParameterByIn} from '../helper/swaggerExample';

const renderParameter = (param, definitions, key) => {
    if (param.value === null) return null;

    if (param.in === "body") {
        return <div key={key}>
            <JSONPretty json={param.value}/>
        </div>
    } else if (param.in === undefined && typeof param.value === "object") {
        return <div key={key}>
            <JSONPretty json={param.value}/>
        </div>
    } else {
        return <div key={key}>
            <b>{param.name}</b> {param.name}={param.value}
        </div>
    }
};

const ParameterSections = (props) => {
    const parametersByIn = sortParameterByIn(props.parameters);

    return <div>
        {Object.keys(parametersByIn).map((section) =>
            <div key={section}>
                <span className="parameterSection">{inDescription(section)}</span>
                <div>
                    {exampleParameters({definitions: props.definitions}, parametersByIn[section]).map((paramExample) => renderParameter(paramExample, props.definitions, `p-${section}-${paramExample.name}`))}
                </div>
            </div>
        )}
    </div>
};

const ResponsesByCode = (props) => {
    return <div><span className="parameterSection">Response</span>
        {Object.keys(props.responses).map((code) =>
            <div key={code}>
                <span>{code} {props.responses[code].description ? `- ${props.responses[code].description}` : ""}</span>
                <div>
                        {renderParameter(exampleParameters({definitions: props.definitions}, [props.responses[code]])[0], props.definitions)}
                </div>
            </div>
        )}

    </div>
};

export const SwaggerRequestExample = (props) => {

    return <div>
        <ParameterSections parameters={props.parameters} definitions={props.definitions}/>
    </div>
};

export const SwaggerResponseExample = (props) => {
    return <div>
        <ResponsesByCode responses={props.responses} definitions={props.definitions}/>
    </div>
};