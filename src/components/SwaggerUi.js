import React, {Component} from 'react';
import PropTypes from 'prop-types';
import '../Swagger.css';
import SwaggerMethodType from './SwaggerMethodType';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col, Container } from 'reactstrap';
import 'react-tabs/style/react-tabs.css';
import {SwaggerRequestExample, SwaggerResponseExample} from './SwaggerTypeExample';
import {inDescription, sortParameterByIn} from '../helper/swaggerExample';
import {SwaggerRequestExperiment} from "./SwaggerTypeExperiment";
import Scopes from "./Scopes";
import Curl from "./Curl";
import classnames from 'classnames';


class TabContainer extends Component {
  render() {
    return (
      <TabPane tabId={this.props.tabId}>
        <Container fluid>
          <Row>
            <Col xs={12}>
              {this.props.children}
            </Col>
          </Row>
        </Container>
      </TabPane>
    );
  }
}

class RequestBodyContainer extends Component {
    render() {
        return <div>
            <span className="requestBodyContainer">REQUEST BODY</span>
            {this.props.children}
        </div>
    }
}


class SwaggerPathMethodParam extends Component {
    static PropTypes = {
        parameter: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired
    };

    renderParam() {
        const style =
            this.props.parameter.in === "body"
                ? {}
                : {borderBottom: "1px solid #DDDDDD", paddingTop: "10px", paddingBottom: "10px"};

        const isBody = this.props.parameter.in === "body";

        return <Container fluid style={style}><Row>
            {[
                <Col key="r0" md={isBody ? 12 : 6}>
                    { isBody
                        ? [
                            <span key="0" className="paramDescription">{this.props.parameter.description}</span>
                            ,
                            <SwaggerMethodType key="1" style={{display: "inline"}} typeName={this.props.parameter.name}
                                               type={this.props.parameter}
                                               definitions={this.props.definitions}/>
                        ]

                        : <b className="float-right">{this.props.parameter.name}</b>
                    }

                </Col>
                ,
                !isBody ?
                    <Col key="r1" md={6}>
                        {this.props.parameter.description ? <p>{this.props.parameter.description} <br /></p> : null}
                        <SwaggerMethodType style={{display: "inline"}} typeName={this.props.parameter.name}
                                           type={this.props.parameter}
                                           definitions={this.props.definitions}/>
                    </Col>
                    : null
            ]}
        </Row > </Container >


    }

    render() {
        return this.props.parameter.in === "body"
            //? <RequestBodyContainer>{this.renderParam()}</RequestBodyContainer>
            ? this.renderParam()
            : this.renderParam()

    }


}

class SwaggerParamTable extends Component {
    render() {
        return <Container fluid>
            {this.props.children}
        </Container>
    }
}

class SwaggerPathMethodParams extends Component {
    static PropTypes = {
        parameters: PropTypes.arrayOf(PropTypes.object).isRequired,
        definitions: PropTypes.object.isRequired

    };

    renderParams() {
        const parametersByIn = sortParameterByIn(this.props.parameters);

        return Object.keys(parametersByIn).map((section) =>
            <div key={section}>
                <span className="parameterSection">{inDescription(section)}</span>
                <div>
                    {parametersByIn[section].map((param) =>
                        <SwaggerPathMethodParam key={`mp-${section}-${param.name}`} parameter={param}
                                                definitions={this.props.definitions}/>
                    )}
                </div>
            </div>
        )
    }

    render() {
        if (this.props.parameters && this.props.parameters.length) {
            return <div className="paramsTable">
                <SwaggerParamTable>{this.renderParams()}</SwaggerParamTable>
            </div>
        } else {
            return null;
        }

    }
}

class SwaggerPathMethodResponse extends Component {
    static PropTypes = {
        code: PropTypes.string.isRequired,
        response: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired

    };

    renderResponseSchema() {
        return this.props.response.schema
            ? <SwaggerMethodType typeName="" type={this.props.response.schema} definitions={this.props.definitions}/>
            : <div></div>;
    }

    render() {
        return <div className="responses">
            <div>{this.props.code}: {this.props.response.description}</div>
            {this.renderResponseSchema()}
        </div>
    }
}

class SwaggerPathMethodResponses extends Component {
    static PropTypes = {
        responses: PropTypes.object.isRequired,
        definitions: PropTypes.object.isRequired

    };

    renderResponses() {
        return this.props.responses
            ? Object.keys(this.props.responses).map((responseCode) => <SwaggerPathMethodResponse key={responseCode}
                                                                                                 code={responseCode}
                                                                                                 response={this.props.responses[responseCode]}
                                                                                                 definitions={this.props.definitions}/>)
            : null;
    }

    render() {
        return <div className="paramsTable"><span className="parameterSection">Response</span>
            <SwaggerParamTable>{this.renderResponses()}</SwaggerParamTable>
        </div>
    }
}

class SwaggerPathMethod extends Component {
    static PropTypes = {
        method: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        summary: PropTypes.string.isRequired,
        operationId: PropTypes.string.isRequired,
        tags: PropTypes.arrayOf(PropTypes.string).isRequired,
        parameters: PropTypes.arrayOf(PropTypes.object).isRequired,
        responses: PropTypes.object.isRequired,
        path: PropTypes.string.isRequired,
        swaggerDoc: PropTypes.object.isRequired,
        runExperiment: PropTypes.func.isRequired,
        oauth2ClientId: PropTypes.string.isRequired,
        scopes: PropTypes.array.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {tab: "example"};
    };

    setTab(tab) {
        this.setState({tab});
    }

    pathToUrl(path) {
        const spec = this.props.swaggerDoc;
        return `${spec.host}${spec.basePath}${path}`;
    }

    uppercase(s) {
        return s.toUpperCase();
    }

    renderLargeHeading() {
        return <Col lg={12} className="hidden-md-down">
            <Container fluid>
                <Row style={{display: "flex", marginLeft: "-30px", marginRight: "-30px"}}>
                    <Col lg={6} className="methodHeadCol">
                        <span className="methodTitle">{this.props.summary || this.props.swaggerDoc.description}</span>
                    </Col>
                    <Col lg={6} className="methodHeadCol exampleCol">
                        <Row>
                          <Col>
                            <span className="methodTitle">{this.uppercase(this.props.method)} {this.props.path}</span>
                          </Col>
                          <Col xs="auto">
                            <Scopes>
                              {this.props.scopes.map(scope => <div>{scope}</div>)}
                            </Scopes>
                          </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </Col>
    }

    renderSpecCol() {
        return <Col lg={6} xs={12}>
            <Container fluid>
                <Row style={{display: "flex"}} className="hidden-lg-up">
                    <Col sm={12} className="methodHeadCol">
                        <span className="methodTitle">{this.props.summary || this.props.swaggerDoc.description}</span>
                    </Col>
                </Row>
                <Row style={{display: "flex"}}>
                    <Col sm={12}>
                        <span className="methodSummary">{this.props.swaggerDoc.description || this.props.summary}</span>
                    </Col>
                </Row>
                <Row style={{display: "flex"}}>
                    <Col sm={12} >
                        <SwaggerPathMethodParams parameters={this.props.parameters}
                                                 definitions={this.props.swaggerDoc.definitions}/>

                    </Col>
                </Row><Row style={{display: "flex"}}>
                <Col sm={12}>
                    <SwaggerPathMethodResponses responses={this.props.responses}
                                                definitions={this.props.swaggerDoc.definitions}/>
                </Col>
            </Row>
            </Container>
        </Col>
    }

    renderTabSelector() {
      return (
        <Container fluid>
          <Row style={{display:"flex"}}>
            <Col sm={12}>
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.tab === 'example' })}
                    onClick={() => { this.setTab('example') }}
                  >
                    Example
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.tab === 'curl' })}
                    onClick={() => { this.setTab('curl') }}
                  >
                    Curl
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: this.state.tab === 'tryOut' })}
                    onClick={() => { this.setTab('tryOut') }}
                  >
                    Try out
                  </NavLink>
                </NavItem>
              </Nav>
            </Col>
          </Row>
        </Container>
      );
    }

    renderExample() {
      return (
        <TabContainer tabId="example">
          <Container fluid>
            <Row style={{display: "flex"}} className="hidden-lg-up">
              <Col sm={12} className="methodHeadCol exampleCol">
                <span className="methodBox" style={{display: "none"}}>{this.props.method}</span>
                <span className="methodTitle">{this.uppercase(this.props.method)} {this.props.path}</span>
              </Col>
            </Row>
            <Row style={{display: "flex"}}>
              <Col sm={12}>
                <SwaggerRequestExample parameters={this.props.parameters}
                  definitions={this.props.swaggerDoc.definitions}/>
              </Col>

            </Row>
            <Row style={{display: "flex"}}>
              <Col sm={12}>
                <SwaggerResponseExample responses={this.props.responses}
                  definitions={this.props.swaggerDoc.definitions}/>
              </Col>

            </Row>
          </Container>
        </TabContainer>
      );
    }

    renderCurl() {
      return (
        <TabContainer tabId="curl">
          <Curl
            spec={this.props.swaggerDoc}
            operationId={this.props.operationId}
            method={this.props.method}
            pathName={this.props.path}
            parameters={this.props.parameters}
            securities={{authorized: {oauth2: { token: { access_token: "<TOKEN>"}}}}}
            />
          </TabContainer>
      );
    }

    renderTryOut() {
      return (
        <TabContainer tabId="tryOut">
          <SwaggerRequestExperiment
            method={this.props.method}
            path={this.props.path}
            operationId={this.props.operationId}
            parameters={this.props.parameters}
            definitions={this.props.swaggerDoc.definitions}
            swaggerDoc={this.props.swaggerDoc}
            consumes={this.props.swaggerDoc.consumes}
            runExperiment={this.props.runExperiment}
            oauth2ClientId={this.props.oauth2ClientId}
          />
        </TabContainer>
      );
    }

    renderExampleCol() {
        return <Col lg={6} xs={12} className="exampleCol" >
            {this.renderTabSelector()}
            <TabContent activeTab={this.state.tab}>
              {this.renderExample()}
              {this.renderCurl()}
              {this.renderTryOut()}
            </TabContent>
        </Col>
    }

    render() {

        return <div id={this.props.operationId} className="method">
            <Container fluid>
                <Row style={{display: "flex"}}>
                    {this.renderLargeHeading()}
                    {this.renderSpecCol()}
                    {this.renderExampleCol()}
                </Row>
            </Container>
        </div>
    }
}

class SwaggerPath
    extends Component {
    static PropTypes = {
        methods: PropTypes.object.isRequired,
        name: PropTypes.string.isRequired,
        swaggerDoc: PropTypes.object.isRequired,
        runExperiment: PropTypes.func.isRequired,
        oauth2ClientId: PropTypes.string.isRequired
    };

    /*
     <div class="opblock-tag-section is-open"><h4 class="opblock-tag"><span>pet</span><small>Everything about your Pets</small><button class="expand-operation" title="Expand operation"><svg class="arrow" width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#large-arrow-down"></use></svg></button></h4><div style="height: auto; border: none; margin: 0px; padding: 0px;"><!-- react-text: 599 --> <!-- /react-text --><div class="opblock opblock-post" id="operations,post-/pet,pet"><div class="opblock-summary opblock-summary-post"><span class="opblock-summary-method">POST</span><span class="opblock-summary-path"><span>/pet</span><!-- react-empty: 605 --></span><div class="opblock-summary-description">Add a new pet to the store</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 721 --></div><div class="opblock opblock-put" id="operations,put-/pet,pet"><div class="opblock-summary opblock-summary-put"><span class="opblock-summary-method">PUT</span><span class="opblock-summary-path"><span>/pet</span><!-- react-empty: 620 --></span><div class="opblock-summary-description">Update an existing pet</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 722 --></div><div class="opblock opblock-get" id="operations,get-/pet/findByStatus,pet"><div class="opblock-summary opblock-summary-get"><span class="opblock-summary-method">GET</span><span class="opblock-summary-path"><span>/pet/findByStatus</span><!-- react-empty: 635 --></span><div class="opblock-summary-description">Finds Pets by status</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 723 --></div><div class="opblock opblock-deprecated" id="operations,get-/pet/findByTags,pet"><div class="opblock-summary opblock-summary-get"><span class="opblock-summary-method">GET</span><span class="opblock-summary-path__deprecated"><span>/pet/findByTags</span><!-- react-empty: 650 --></span><div class="opblock-summary-description">Finds Pets by tags</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 724 --></div><div class="opblock opblock-get" id="operations,get-/pet/{petId},pet"><div class="opblock-summary opblock-summary-get"><span class="opblock-summary-method">GET</span><span class="opblock-summary-path"><span>/pet/{petId}</span><!-- react-empty: 665 --></span><div class="opblock-summary-description">Find pet by ID</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 725 --></div><div class="opblock opblock-post" id="operations,post-/pet/{petId},pet"><div class="opblock-summary opblock-summary-post"><span class="opblock-summary-method">POST</span><span class="opblock-summary-path"><span>/pet/{petId}</span><!-- react-empty: 680 --></span><div class="opblock-summary-description">Updates a pet in the store with form data</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 726 --></div><div class="opblock opblock-delete" id="operations,delete-/pet/{petId},pet"><div class="opblock-summary opblock-summary-delete"><span class="opblock-summary-method">DELETE</span><span class="opblock-summary-path"><span>/pet/{petId}</span><!-- react-empty: 695 --></span><div class="opblock-summary-description">Deletes a pet</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 727 --></div><div class="opblock opblock-post" id="operations,post-/pet/{petId}/uploadImage,pet"><div class="opblock-summary opblock-summary-post"><span class="opblock-summary-method">POST</span><span class="opblock-summary-path"><span>/pet/{petId}/uploadImage</span><!-- react-empty: 710 --></span><div class="opblock-summary-description">uploads an image</div><button class="authorization__btn unlocked"><svg width="20" height="20"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#unlocked"></use></svg></button></div><!-- react-empty: 728 --></div><!-- react-text: 720 --> <!-- /react-text --></div></div>
     */

    extractScopes = security => {
      return security
        ? security
          .filter(s => s.oauth2)
          .map(s => s.oauth2)
          .reduce((acc, scope) => acc.concat(scope), [])
        : [];
    }

    renderMethods() {
        const methods = this.props.methods;
        return Object.keys(this.props.methods).map((methodName) => <SwaggerPathMethod
            key={methodName}
            method={methodName} description={methods[methodName].description}
            operationId={methods[methodName].operationId}
            scopes={this.extractScopes(methods[methodName].security)}
            parameters={methods[methodName].parameters} responses={methods[methodName].responses}
            path={this.props.name}
            swaggerDoc={this.props.swaggerDoc}
            summary={methods[methodName].summary}
            runExperiment={this.props.runExperiment}
            oauth2ClientId={this.props.oauth2ClientId}
        />)
    }

    render() {
        return <div className="path">
            {this.renderMethods()}

        </div>;
    }
}

class SwaggerPaths extends Component {
    static PropTypes = {
        paths: PropTypes.arrayOf(PropTypes.object).isRequired,
        swaggerDoc: PropTypes.object.isRequired,
        runExperiment: PropTypes.func.isRequired,
        oauth2ClientId: PropTypes.string.isRequired
    };

    renderPaths = (paths) => {
        return Object.keys(paths).map((pathName) => {
            return <SwaggerPath key={pathName} name={pathName} methods={paths[pathName]} swaggerDoc={this.props.swaggerDoc}
                                runExperiment={this.props.runExperiment}  oauth2ClientId={this.props.oauth2ClientId}/>
        })
    };

    render() {
        return <div className="swaggerBase">{this.renderPaths(this.props.paths)}</div>;
    }
}

class SwaggerTag extends Component {
    static PropTypes = {
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired
    };

    render() {
        return <div className="tag">
            <Container fluid>
                <Row className="no-gutters" style={{display: "flex"}}>
                    <Col lg={6}>
                        <span className="tagHeading">{this.props.name}</span>
                        <span className="tagDescription">{this.props.description}</span>
                    </Col>
                    <Col lg={6} className="exampleCol clearfix hidden-md-down">
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {this.props.children}
                    </Col>
                </Row>
            </Container>
        </div>
    }
}

export default class SwaggerUi extends Component {

    static PropTypes = {
        spec: PropTypes.object.isRequired,
        tags: PropTypes.arrayOf(PropTypes.string).isRequired,
        runExperiment: PropTypes.func.isRequired,
        oauth2ClientId: PropTypes.string.isRequired
    };

    tagFilter = (tagName) => (path) => {
        return Object.keys(path)
                .filter((method) => path[method].tags.indexOf(tagName) !== -1).length > 0;
    };

    objectFilter = (filter) => (object) => {
        const filteredKeys = Object.keys(object)
            .filter((key) => filter(object[key]));

        const ret = {};

        filteredKeys.forEach((key) => {
            ret[key] = object[key];
        });

        return ret;
    };

    renderTags(swaggerDoc) {

        const tagDescription = (name) => {
            const tags = swaggerDoc.tags.filter((tag) => tag.name === name)
            return tags.length > 0 ? tags[0].description : ""
        };

        const tagsToShow = (this.props.tags)
            ? this.props.tags.map((t) => ({name: t, description: tagDescription(t)}))
            : swaggerDoc.tags;

        return tagsToShow.map((tag) => (
            <SwaggerTag key={tag.name} name={tag.name} description={tag.description}>
                <SwaggerPaths paths={this.objectFilter(this.tagFilter(tag.name))(swaggerDoc.paths)}
                              swaggerDoc={swaggerDoc} runExperiment={this.props.runExperiment} oauth2ClientId={this.props.oauth2ClientId}/>
            </SwaggerTag>
        ));
    }

    render() {


        return this.props.spec.paths
            ? <div>
                {this.renderTags(this.props.spec)}
            </div>
            : <div>empty doc</div>

    }


}
