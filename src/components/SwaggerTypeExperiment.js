import React, {Component} from 'react';
import JSONPretty from 'react-json-pretty';
import {exampleParameters, inDescription, sortParameterByIn} from '../helper/swaggerExample';
import Login from "./Login";
import {connect} from "react-redux";

const renderParameter = (param, updateValue) => {
    if (param.value === null) return null;

    const onChange = (e) => {
        updateValue(param, e.target.value);
    };

    if (param.in === "body") {
        return [
            <label htmlFor={param.name}>{param.name}</label>
            ,
            <textarea className="form-control" defaultValue={JSON.stringify(param.value)} onChange={onChange}>
            </textarea>
        ]
    } else if (param.in === undefined) {
        return [
            <label for={param.name}>{param.name}</label>
            ,
            <textarea className="form-control" defaultValue={JSON.stringify(param.value)} onChange={onChange}>
            </textarea>
        ]
    } else {
        return [
            <label htmlFor={param.name}>{param.name}</label>
            ,
            <input className="form-control" type="text" name={param.name} defaultValue={param.value} id={param.name}
                   placeholder="" onChange={onChange}/>
        ]
    }
};

const mapObject = (obj, func) => Object.keys(obj).reduce(function (previous, current) {
    previous[current] = func(obj[current]);
    return previous;
}, {});

const ParameterSections = (props) => {
    const parametersByIn = sortParameterByIn(props.parameters);

    const values = mapObject(parametersByIn, (section) => exampleParameters({definitions: props.definitions}, section));

    const updateValue = (param, newValue) => {
        values[param.in].filter(p => p.name === param.name)[0].value = newValue;
    };

    const runValues = {};
    Object.keys(values).forEach((sectionName) => {
        values[sectionName].forEach((elem) => {
            runValues[elem.name] = elem.value;
        })
    });

    const run = (e) => {
        e.preventDefault();
        props.runExperiment(runValues);
    };

    return <form onSubmit={run}>
        <div>
            {Object.keys(parametersByIn).map((sectionName) =>
                <div key={sectionName}>
                    <span className="parameterSection">{inDescription(sectionName)}</span>
                    <div className="form-group">
                        {values[sectionName].map((paramExample) => renderParameter(paramExample, updateValue))}
                    </div>
                </div>
            )}
            <button className="btn btn-primary">Submit</button>
        </div>
    </form>
};

const ExperimentFailed = (props) => {
    console.log("failed:");
    console.log(props.response.message);
    const message = props.response.message ? `: ${props.response.message}` : "";

    return <div className="failedTryout">
        <div>API request failed{message}</div>
        <div>{props.response.status} - {props.response.statusText}</div>
        <div>{props.response.text}</div>
    </div>
};

const ExperimentSucceeded = (props) => {
    const contentType = props.response.headers ? props.response.headers["content-type"] : null;
    return <div className="succeededTryout">
        <div>{props.response.status} - {props.response.statusText}</div>
        {contentType === "application/json"
            ? <JSONPretty id="json-pretty" json={props.response.text}/>
            : <div>{props.response.text}</div>
        }
    </div>
};

const ExperimentResponse = (props) => {
    return props.response.ok
        ? <ExperimentSucceeded response={props.response}/>
        : <ExperimentFailed response={props.response}/>
};

class UnconnectedSwaggerRequestExperiment extends Component {

    state = {response: {ok: true}};

    getSecurity() {
        const securities = this.props.swaggerDoc.paths[this.props.path][this.props.method].security;

        const securityDefinition = (securities) => {
            const name = Object.keys(securities[0])[0];
            return this.props.swaggerDoc.securityDefinitions[name];
        };

        if (securities) {
            return securityDefinition(securities);
        } else {
            return null;
        }

    }

    render() {
        const responseReceived = (response, error) => {
            if (error) {
                this.setState({response: error});
            } else {
                this.setState({response: response});
            }
        };

        const run = (params) => {
            this.props.runExperiment(this.props.swaggerDoc, this.props.method, this.props.path, params, this.props.securities, responseReceived)
        };


        const security = this.getSecurity();
        const authorizationUrl = () => {
            if (!security) {
                return null;
            }

            const isCompleteUrl = (url) => !url || (url.indexOf("http://") > -1 || url.indexOf("https://") > -1);
            const joinHostAndPath = (host, path) =>
                (path.indexOf('/') === 0)
                    ? `${host}${path}`
                    : `${host}/${path}`;

            return isCompleteUrl(security.authorizationUrl)
                ? security.authorizationUrl
                : `${this.props.swaggerDoc.schemes[0]}://${joinHostAndPath(this.props.swaggerDoc.host, security.authorizationUrl)}`;
        };

        return <div>
            <Login security={{...security, authorizationUrl: authorizationUrl()}} client={this.props.oauth2ClientId}/>
            <ParameterSections
              parameters={this.props.parameters}
              definitions={this.props.definitions}
              runExperiment={run}
              spec={this.props.swaggerDoc}
              operationId={this.props.operationId}
              method={this.props.method}
              pathName={this.props.path}
              securities={this.props.securities}
            />
            <ExperimentResponse response={this.state.response}/>
        </div>
    }
}

const mapStateToProps = (state) => ({
    securities: {authorized: {oauth2: { token: { access_token: state.auth.token}}}} ,
    state: state
});


export const SwaggerRequestExperiment = connect(mapStateToProps, null)(UnconnectedSwaggerRequestExperiment);
