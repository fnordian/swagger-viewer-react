import React from 'react';
import ReactDOM from 'react-dom';
import thunk from 'redux-thunk';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import {applyMiddleware, combineReducers, createStore} from "redux";
import {connect, Provider} from "react-redux";
import experiment from './reducers/experiment';
import {runExperiment} from "./reducers/experiment";
import {authMiddleware, authReducer as auth} from "@fuglu/redux-implicit-oauth2";

let store = createStore(
    combineReducers({
        experiment,
        auth
    }),
    {},
    applyMiddleware(thunk, authMiddleware)
);

const mapDispatchToProps = {
    runExperiment
};

let ConnectedApp = connect(null, mapDispatchToProps)(App);

ReactDOM.render(
    <div>
        <Provider store={store}>
            <ConnectedApp swaggerUrl='pets-swagger.json'/>
        </Provider>
    </div>, document.getElementById('root'));
registerServiceWorker();
