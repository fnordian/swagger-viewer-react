import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './App.css';
import SwaggerUi from "./components/SwaggerUi";

class App extends Component {
    static propTypes = {
        swaggerUrl: PropTypes.string.isRequired,
        swaggerModelUpdate: PropTypes.func,
        tagFilter: PropTypes.arrayOf(PropTypes.string),
        methodFilter: PropTypes.arrayOf(PropTypes.string),
        auth: PropTypes.object.isRequired
    };

    static defaultProps = {
        swaggerModelUpdate: () => {},
        tagFilter: [],
        methodFilter: []
    };

    state = { swaggerText: '{}' };

    swaggerText = `{}`;

    get = url => {

        console.warn("fetching " + url);
        return fetch(`${url}`, {
            credentials: "include"
        });
    };

//    parseJson = result => result.json();

    fetchJson = (filename) =>
        this.get(filename);


    setSwaggerText(json) {
        const self = this;
        json.text().then((text) => {
            this.setState({swaggerText: text});
            if (this.props.swaggerModelUpdate) {
                this.props.swaggerModelUpdate(JSON.parse(text));
            }
        });

    }

    componentDidMount() {
        const self = this;
        this.fetchJson(this.props.swaggerUrl).then((json) => self.setSwaggerText(json));
    }

    render() {
        return <SwaggerUi spec={JSON.parse(this.state.swaggerText)} tags={this.props.tags} runExperiment={this.props.runExperiment} oauth2ClientId="hocusdocus" />;
    }
}



export default App;
