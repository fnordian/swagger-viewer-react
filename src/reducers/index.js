export {createExperiment as runExperiment} from './experiment'

import experimentStore from './experiment'

export default  (initialState) =>
    createStore(
        combineReducers({
            // other reducers
            experimentStore,
            auth
        }),
        initialState,
        applyMiddleware(
            // other middleware
            thunk,
            authMiddleware
        )
    )

