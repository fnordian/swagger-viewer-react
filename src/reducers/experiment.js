import Swagger from 'swagger-client'

const RUN_EXPERIMENT = 'RUN_EXPERIMENT';

export const createExperiment = (spec, method, pathName, parameters, securities, onResponse) => {

    securities.specSecurity = Object.values(spec.securityDefinitions);

    const params = {
        spec,
        pathName,
        method,

        parameters, // _named_ parameters in an object, eg: { petId: 'abc' }
        securities, // _named_ securities, will only be added to the request, if the spec indicates it. eg: {apiKey: 'abc'}
        //requestContentType,
        //responseContentType,

    };

    const res = Swagger.execute({...params}).then(onResponse, (reason) => {
        onResponse(null, reason);
    });

};

export const runExperiment = (spec, method, pathName, params, securities, onResponse) => ({
    type: RUN_EXPERIMENT,
    spec, method, pathName, params, securities, onResponse
});


export default (state = {activeExperiment: null}, action) => {

    if (action.type === RUN_EXPERIMENT) {
        console.log(action.params);
        console.log(`action ${action.securities}`);
        createExperiment(action.spec, action.method, action.pathName, action.params, action.securities, action.onResponse);
        return {
            ...state,
            activeExperiment: {}
        }
    } else {
        return state;
    }
}