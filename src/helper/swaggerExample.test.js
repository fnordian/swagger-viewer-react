import example, {resolveRef} from './swaggerExample';
import fs from 'fs';

const spec = JSON.parse(fs.readFileSync("./public/pets-swagger.json"));

it('refs are resolved', () => {
    const ref = resolveRef(spec, "#/definitions/Pet");
    expect(ref.type).toEqual('object');
});

it('returns params of path', () => {
    const {params} = example(spec, '/pet', 'post');
    expect(params.length).toEqual(1);
});

it('returns example for integer', () => {
    const {params} = example(spec, '/pet', 'post');
    expect(params[0].value.id).toEqual(1);
});

it('returns example for string', () => {
    const {params} = example(spec, '/pet', 'post');
    expect(params[0].value.name).toEqual('doggie');
});

it('returns example for array', () => {
    const {params} = example(spec, '/pet', 'post');
    expect(params[0].value.photoUrls.length).toEqual(1);
});


it('returns example for deep structure', () => {
    const {params} = example(spec, '/pet', 'post');
    expect(params[0].value.tags[0].id).toEqual(1);
    console.log(params);
});