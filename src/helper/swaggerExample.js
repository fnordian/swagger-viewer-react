const exampleObject = (swagger, object) => {
    const ret = {};

    for (var property in object.properties) {
        ret[property] = exampleValue(swagger, object.properties[property]);
    }


    return ret;
};

const exampleEnum = (swagger, t) => {
    return (t.enum && t.enum.length)
        ? t.enum[0]
        : null;
};

const exampleInteger = (swagger, int) => {
    return 1;
};

const exampleString = (swagger, s) => {
    return s.example || exampleEnum(swagger, s) || "foo";
};

const exampleBoolean = (swagger, s) => {
    return s.example || true;
};

const exampleArray = (swagger, a) => {
    const arrayType = valueType(swagger, a.items);
    return [exampleValue(swagger, arrayType)];
};

export const resolveRef = (swagger, ref) => {
    const refPath = ref.split('/');
    const walkPath = (node, path) => path.length ? walkPath(node[path[0]], path.splice(1, path.length - 1)) : node;

    return walkPath(swagger, refPath.splice(1, refPath.length - 1));
};


export const valueType = (swagger, value) => {
    if (value.schema && value.schema['$ref']) {
        return resolveRef(swagger, value.schema['$ref'])
    } else if (value['$ref']) {
        return resolveRef(swagger, value['$ref']);
    } else if (value.schema) {
        return value.schema;
    } else {
        return value;
    }
};

const exampleValue = (swagger, value) => {
    const t = valueType(swagger, value);
    switch (t.type) {
        case 'object':
            return exampleObject(swagger, t);
        case 'integer':
            return exampleInteger(swagger, t);
        case 'string':
            return exampleString(swagger, t);
        case 'array':
            return exampleArray(swagger, t);
        case 'boolean':
            return exampleBoolean(swagger, t);
        default:
            return null;
    }
};

export const exampleParameters = (swagger, parameters) => {
    return parameters.map((param) => ( {
        value: exampleValue(swagger, param),
        in: param.in,
        description: param.description,
        name: param.name

    }));
};

const example = (swagger, path, method) => {

    const p = swagger.paths[path][method];

    const params = exampleParameters(swagger, p.parameters);

    return {params};

};


export const sortParameterByIn = (params) => {
    const ret = {};
    for (let i in params) {
        if (!ret[params[i].in]) ret[params[i].in] = [];
        ret[params[i].in].push(params[i]);
    }

    return ret;
};

export const inDescription = (inShortDescription) => {
    switch (inShortDescription) {
        case "query":
            return "Query parameter";
        case "path":
            return "Path element";
        case "body":
            return "Request body";
        case "header":
            return "Request header";
        case "formData":
            return "Form data";
        default:
            return inShortDescription;
    }
};


export default example