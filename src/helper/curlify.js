import Swagger from 'swagger-client'
import { exampleParameters, sortParameterByIn } from '../helper/swaggerExample';

const mapObject = (obj, func) => Object.keys(obj).reduce((previous, current) => {
  previous[current] = func(obj[current]);
  return previous;
}, {});

const convertParameters = (spec, parameters) => {
  const parametersByIn = sortParameterByIn(parameters);

  const values = mapObject(parametersByIn, (section) => exampleParameters({definitions: spec.definitions}, section));

  const runValues = {};
  Object.keys(values).forEach((sectionName) => {
    values[sectionName].forEach((elem) => {
      runValues[elem.name] = elem.value;
    })
  });

  return runValues;
}

const buildRequest = (spec, operationId, method, pathName, parameters, securities) => {
  try {
   return Swagger.buildRequest({
      spec,
      operationId,
      method,
      pathName,
      parameters: convertParameters(spec, parameters),
      securities,
    });
  } catch (e) {
  }

  try {
   return Swagger.buildRequest({
      spec,
      operationId: `${method}-${pathName.toLowerCase()}`,
      method,
      pathName,
      parameters: convertParameters(spec, parameters),
      securities,
    });
  } catch (e) {
  }

  return "";
};

export default (spec, operationId, method, pathName, parameters, securities) => {
  const request = buildRequest(spec, operationId, method, pathName, parameters, securities);

  if (!request) {
    return "";
  }

  const curlified = []
  const headers = request.headers;
  const isFormData = Object.values(headers).some(value => value.match('form'));

  curlified.push("curl")
  curlified.push(`--request ${request.method}`)

  if (headers) {
    Object.keys(headers).forEach((key) => {
      const value = headers[key];
      curlified.push(`--header '${key}: ${value}'`)
    });
  }

  if (request.body) {
    if (isFormData) {
      curlified.push(`--data '${request.body}'`)
    } else {
      const json = JSON.stringify(request.body).replace(/\\n/g, "");
      curlified.push(`--data '${json}'`)
    }
  }

  curlified.push(`'${request.url}'`)

  return curlified.join(" \\\n")
}
